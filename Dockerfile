FROM openjdk:11
ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk-amd64
ADD https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-8.9.0.43852.zip /tmp
RUN unzip /tmp/sonarqube-8.9.0.43852.zip -d /opt/
ADD Entrypoint.sh /opt/sonarqube-8.9.0.43852
WORKDIR /opt/sonarqube-8.9.0.43852
EXPOSE 9000
RUN groupadd sonarqube && useradd -ms /bin/bash -g sonarqube sonarqube
RUN chown -R sonarqube:sonarqube /opt/sonarqube-8.9.0.43852
USER sonarqube
ENTRYPOINT ["/bin/bash", "Entrypoint.sh"]
