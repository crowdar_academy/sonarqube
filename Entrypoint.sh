#!/bin/bash
sed -i -e "s/#sonar.jdbc.username=/sonar.jdbc.username=${SONAR_JDBC_USERNAME}/" /opt/sonarqube-8.9.0.43852/conf/sonar.properties
sed -i -e "s/#sonar.jdbc.password=/sonar.jdbc.password=${SONAR_JDBC_PASSWORD}/" /opt/sonarqube-8.9.0.43852/conf/sonar.properties
echo "sonar.jdbc.url=${SONAR_JDBC_URL}" >> /opt/sonarqube-8.9.0.43852/conf/sonar.properties
java -jar /opt/sonarqube-8.9.0.43852/lib/sonar-application-8.9.0.43852.jar -Dsonar.log.console=true
